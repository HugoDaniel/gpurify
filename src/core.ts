/// <reference lib="dom" />
/// <reference lib="dom.iterable" />
import Immutable from "immutable";
import { debounce } from "./common";

type CreateContextOpts = {
  /** Defaults to 'premultiplied' */
  alphaMode?: GPUCanvasAlphaMode;
  /** Handler for window resize events */
  onResize?: (newContext: Context) => void;
  /** Defaults to 'high-performance' */
  powerPreference?: GPUPowerPreference;
};

class ErrorWebGPUNotSupported {
  readonly name = "WebGPU is not supported in this browser";
  constructor(readonly details?: string) {}
}
class ErrorWebGPUNoAdapterFound {
  readonly name = "No adapter found";
  constructor(readonly details?: string) {}
}
class ErrorWebGPUNoDevice {
  readonly name = "No device returned from adapter";
  constructor(readonly details?: string) {}
}
class ErrorWebGPUNoCanvasContext {
  readonly name = "Unable to get webgpu context from canvas";
  constructor(readonly details?: string) {}
}

export type ContextError =
  | ErrorWebGPUNotSupported
  | ErrorWebGPUNoAdapterFound
  | ErrorWebGPUNoDevice
  | ErrorWebGPUNoCanvasContext;

export type Context = Immutable.MapOf<{
  device: GPUDevice;
  context: GPUCanvasContext;
  canvas: HTMLCanvasElement;
  aspect: number;
  devicePixelRatio: number;
  width: number;
  height: number;
  resizeHandler: () => void;
}>;

/**
 * Initializes a new rendering context on the given canvas.
 */
export async function createContext(
  canvas: HTMLCanvasElement,
  options: CreateContextOpts = {}
): Promise<Context | ContextError> {
  if (!navigator.gpu)
    return new ErrorWebGPUNotSupported(
      "Maybe you need to enable WebGPU in your browser. Is your version browser up to date? See https://webgpu.io for more information."
    );

  const adapter = await navigator.gpu.requestAdapter({
    powerPreference: options.powerPreference ?? "high-performance",
  });
  if (!adapter) return new ErrorWebGPUNoAdapterFound();

  const device = await adapter.requestDevice();
  if (!device) return new ErrorWebGPUNoDevice();

  const context = canvas.getContext("webgpu");
  if (!context) return new ErrorWebGPUNoCanvasContext();

  const devicePixelRatio = window.devicePixelRatio || 1;
  canvas.width = canvas.clientWidth * devicePixelRatio;
  canvas.height = canvas.clientHeight * devicePixelRatio;
  const aspect = canvas.width / canvas.height;

  const presentationFormat = navigator.gpu.getPreferredCanvasFormat();
  context.configure({
    device,
    format: presentationFormat,
    alphaMode: options.alphaMode ?? "premultiplied",
  });

  const resizeHandler = debounce(() => {
    canvas.width = canvas.clientWidth * devicePixelRatio;
    canvas.height = canvas.clientHeight * devicePixelRatio;
    context.configure({
      device,
      format: presentationFormat,
      alphaMode: options.alphaMode ?? "premultiplied",
    });
    if (options.onResize)
      options.onResize(
        result.withMutations((ctx) =>
          ctx
            .set("width", canvas.clientWidth)
            .set("height", canvas.clientHeight)
            .set("aspect", canvas.clientWidth / canvas.clientHeight)
        )
      );
  }, 100);

  const result = Immutable.Map({
    device,
    context,
    canvas,
    aspect,
    devicePixelRatio,
    width: canvas.clientWidth,
    height: canvas.clientHeight,
    resizeHandler,
  });

  // Initialize the resize handler:
  addEventListener("resize", resizeHandler);

  return result;
}

export function destroyContext(ctx: Context): void {
  window.removeEventListener("resize", ctx.get("resizeHandler"));
  // Additional cleanup tasks can be added here if needed in the future.
}
