
/**
 * Debounce a function.
 * @param fn The function to debounce.
 * @param delay The delay in milliseconds.
 * @returns A debounced version of the function that only executes after the
 * delay has passed.
 */
export function debounce(
  fn: (...args: any[]) => void,
  delay: number
): (...args: any[]) => void {
  let timeoutId: Timer | null = null;

  return function (...args: any[]): void {
    if (timeoutId !== null) {
      clearTimeout(timeoutId);
    }
    timeoutId = setTimeout(() => {
      fn(...args);
      timeoutId = null;
    }, delay);
  };
}
